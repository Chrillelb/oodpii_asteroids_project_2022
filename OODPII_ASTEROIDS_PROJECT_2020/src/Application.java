import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import Commands.BwdCommand;
import Commands.Command;
import Commands.FwdCommand;
import Commands.LeftCommand;
import Commands.RightCommand;
import Modell.Container;
import Modell.CommandInvoker;


/**
 * 
 */

/**
 * @author chris
 *
 */

public class Application extends JFrame implements KeyListener  {
	
	private Timer timer;
	private Timer bulletTimer;
	
	private Container container;
	
	//users sets difficulty in imaginary menu
	private int difficulty = 10; 
	
	Command forward;
	Command backward;
	Command left;
	Command right;
	CommandInvoker shipControl = new CommandInvoker();


	private static final long serialVersionUID = 1L;
	
	public Application() {
		
		this.container = Container.getInstance(difficulty);
		this.add(container);
		this.setSize(1000, 1000);
		this.setBackground(Color.BLACK);
		this.addKeyListener(this);
		this.timer = new Timer();	
		this.bulletTimer = new Timer();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		forward = new FwdCommand(container.getShip());
		backward = new BwdCommand(container.getShip());
		left = new LeftCommand(container.getShip());
		right = new RightCommand(container.getShip());
	
	
	}
	
	
	public void start() {
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				GameLoop();
			}
		}, 0, 100);
		bulletTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				BulletGameLoop();
			}
		}, 0, 1);
	}
	
	public void GameLoop() {
		this.container.updateContainer();
		this.container.repaint();
	}
	
	public void BulletGameLoop() {
		this.container.updateBullet();
		this.container.repaint();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_A) {
			shipControl.setCommand(left);
			shipControl.pressCommand();
		}
		if (e.getKeyCode() == KeyEvent.VK_D) {
			shipControl.setCommand(right);
			shipControl.pressCommand();
		}
		if (e.getKeyCode() == KeyEvent.VK_W) {
			shipControl.setCommand(forward);
			shipControl.pressCommand();
		}
		if (e.getKeyCode() == KeyEvent.VK_S) {
			shipControl.setCommand(backward);
			shipControl.pressCommand();
		}
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			this.container.shoot();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	public int getDifficulty() {
		return difficulty;
	}


	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	
	

}
