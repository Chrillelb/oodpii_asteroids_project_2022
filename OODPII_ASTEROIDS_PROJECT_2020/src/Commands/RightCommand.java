package Commands;
import Modell.Ship;

//Concrete command class
public class RightCommand implements Command {
	
	private Ship ship;

	public RightCommand(Ship ship) {
		this.ship = ship;
	}

	@Override
	public void execute() {
		ship.RotateRight();
	}

}
