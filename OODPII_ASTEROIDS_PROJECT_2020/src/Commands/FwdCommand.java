package Commands;

import Modell.Ship;

//Concrete command class
public class FwdCommand implements Command {
	
	private Ship ship;
	
	
	public FwdCommand(Ship ship) {
		this.ship = ship;
	}

	@Override
	public void execute() {
		ship.moveForward();
	}

}
 