package Commands;
import Modell.Ship;


//Concrete command class
public class LeftCommand implements Command {
	
	private Ship ship;

	public LeftCommand(Ship ship) {
		this.ship = ship;
	}

	@Override
	public void execute() {
		ship.RotateLeft();

	}

}
