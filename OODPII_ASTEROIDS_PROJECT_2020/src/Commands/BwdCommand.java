package Commands;

import Modell.Ship;

//Concrete command class
public class BwdCommand implements Command {

	private Ship ship;
	
	public BwdCommand(Ship ship) {
		this.ship = ship;
	}
	
	@Override
	public void execute() {
		ship.moveBackward();

	}

}
