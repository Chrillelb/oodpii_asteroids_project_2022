package Modell;

import Commands.Command;

public class CommandInvoker {

	//The invoker, takes all commmands and executes them
	private Command command;

	public void setCommand(Command command) {
		this.command = command;
	}
	public void pressCommand() {
		command.execute();
	}
}
