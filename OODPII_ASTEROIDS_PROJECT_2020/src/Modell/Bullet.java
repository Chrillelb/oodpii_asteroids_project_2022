package Modell;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

public class Bullet implements DrawableObject{
	

	private Polygon bullet;
	private boolean isAlive;
	
	private double bulletAngel;

	private Point2D.Double p1 = new Point2D.Double();
	private Point2D.Double p2 = new Point2D.Double();
	private Point2D.Double p3 = new Point2D.Double();
	private Point2D.Double p4 = new Point2D.Double();

	private int width = 15;
	private int height = 5;

	private int[] bulletx;
	private int[] bullety;
	
	private int startX;
	private int startY;

	private AffineTransform at;

	/**
	 * Konstruktor för klassen Bullet
	 * @param startX
	 * @param startY
	 * @param angle
	 */
	public Bullet() {

	}
	
	public void createBullet() {
		
		this.at = new AffineTransform();

		
		System.out.println(this.startX + " " + this.startY);
		
		this.p1.x = startX;
		this.p1.y = startY;

		this.p2.x = p1.x + this.width;
		this.p2.y = p1.y;

		this.p3.x = p1.x + this.width;
		this.p3.y = p1.y + this.height;

		this.p4.x = p1.x;
		this.p4.y = p1.y + this.height;

		at.rotate(Math.toRadians(this.bulletAngel), startX, startY);
		at.transform(p1, p1);
		at.transform(p2, p2);
		at.transform(p3, p3);
		at.transform(p4, p4);

		this.bulletx = new int[] { (int) p1.x, (int) p2.x, (int) p3.x, (int) p4.x };
		this.bullety = new int[] { (int) p1.y, (int) p2.y, (int) p3.y, (int) p4.y };

		this.bullet = new Polygon(this.bulletx, this.bullety, 4);
		
	}
	

	/**
	 * Returnerar kulans vinkel
	 * 
	 * @return int
	 */
	public double getAngle() {
		return bulletAngel;
	}

	/**
	 * Sätter kulans vinkel
	 * 
	 * @param bulletAngel
	 */
	public void setAngle(double bulletAngel) {
		this.bulletAngel = bulletAngel - 90;
	}

	/**
	 * Returnerar en polygon "bullet"
	 * 
	 * @return Polygon
	 */
	public Polygon getBullet() {
		return this.bullet;
	}

	/**
	 * Tar emot en Polygon bullet och sätter den
	 * 
	 * @param bullet
	 */
	public void setBullet(Polygon bullet) {
		this.bullet = bullet;
	}

	/**
	 * Metod f�r att f�r�ndra polygonens position och vinkel
	 */
	
	@Override
	public void move() {
		
		this.p1.x = p1.x - (1   * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p1.y = p1.y + (1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		this.p2.x = p2.x - (1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p2.y = p2.y + (1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		this.p3.x = p3.x - (1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p3.y = p3.y + (1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		this.p4.x = p4.x - (1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p4.y = p4.y + (1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		
		this.bulletx = new int[] { (int) p1.x, (int) p2.x, (int) p3.x, (int) p4.x };
		this.bullety = new int[] { (int) p1.y, (int) p2.y, (int) p3.y, (int) p4.y };

		this.bullet = new Polygon(this.bulletx, this.bullety, 4);
	}


	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	public void draw(Graphics g) {
		
		Graphics2D g2d = (Graphics2D) g;
		AffineTransform tx = new AffineTransform();
		
		Shape polygon = this.bullet;
				
		g2d.setColor(Color.DARK_GRAY);
		g2d.draw(tx.createTransformedShape(polygon));
		
	}

	public Shape getShape() {
		// TODO Auto-generated method stub
		return (Shape) this.bullet;
	}


	@Override
	public double getLocationX() {
		return this.bullet.xpoints[0];
	}


	@Override
	public double getLocationY() {
		return this.bullet.ypoints[0];
	}

	@Override
	public void setStartLocation(Double p1) {
		this.p1 = p1;
	}

	@Override
	public void setLocationX(int i) {
		this.startX = i;
		
	}

	@Override
	public void setLocationY(int i) {
		this.startY = i;
		
	}



}
