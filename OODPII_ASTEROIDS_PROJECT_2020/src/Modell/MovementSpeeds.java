package Modell;

public class MovementSpeeds {
	
	public double GetMovementSpeeds(int randomSpeedGenerator) {
		
		double movementSpeed = 1;
		double accelation = 10;
		
		if(randomSpeedGenerator == -2) {
			movementSpeed = -accelation;
		}
		else if (randomSpeedGenerator == -1) {
			movementSpeed = -accelation;
		}
		else if (randomSpeedGenerator == 0) {
			movementSpeed = accelation;
		}
		else if (randomSpeedGenerator == 1) {
			movementSpeed = accelation;
		}
		else if (randomSpeedGenerator == 2) {
			movementSpeed = accelation;
		}
		
		return movementSpeed;
	}

}
