package Modell;

import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.Point2D;

public interface DrawableObject {

	public void draw(Graphics g);
	
	public Shape getShape();
	
	public double getAngle();

	public void setAngle(double bulletAngle);
	
	public boolean isAlive();
	
	public void setAlive(boolean isAlive);


	
	public double getLocationX();

	public void setStartLocation(Point2D.Double p1);

	public double getLocationY();

	public void setLocationX(int i);


	public void setLocationY(int i);

	public void createBullet();

	public void move();
	
}
