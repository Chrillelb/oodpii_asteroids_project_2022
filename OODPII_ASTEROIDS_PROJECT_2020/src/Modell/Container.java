package Modell;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;


public class Container extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Ship ship;

	private static Container container;
	
	private DrawableObjectCreator drawableObjectCreator = new DrawableObjectCreator();

	private List<DrawableObject> asteroidList = new ArrayList<DrawableObject>();
	private List<DrawableObject> bulletList = new ArrayList<DrawableObject>();
	private Iterator<DrawableObject> asteroidIterator;
	private Iterator<DrawableObject> bulletIterator;
	
	private ShipHandler shipHandler;

	
	
	private Container(int difficulty) {
		this.shipHandler = ShipHandler.getInstance();

		for(int i = 0; i <= difficulty; i++) {
			this.asteroidList.add(this.drawableObjectCreator.createDrawableObject("asteroid"));
		}


	}
	
	public static Container getInstance(int difficulty) {
		if(container == null) {
			container =new Container(difficulty);
		} 
		return container;
	}
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		this.shipHandler.getShip().draw(g);
		this.asteroidList.forEach(asteroid -> {

			asteroid.draw(g);
		});
		this.bulletList.forEach(bullet -> {
			bullet.draw(g);
		});
			
	}
	
	public void updateContainer() {
		
		this.bulletIterator = bulletList.iterator();
		this.asteroidIterator = asteroidList.iterator();

		this.asteroidList.forEach((ast) -> {
			ast.move();
			
			if(ast.getLocationX() >= 1000) {
				ast.setLocationX(0);
			}
			else if (ast.getLocationY() >= 1000) {
				ast.setLocationY(0);
			}
			else if (ast.getLocationX() <= 0) {
				ast.setLocationX(1000);
			}
			else if (ast.getLocationY() <= 0) {
				ast.setLocationY(1000);
			}
			else {
				
			}
			
			
		});
		
		
	}
	
	
	public Ship getShip() {
		return this.shipHandler.getShip();
	}

	public void shoot() {
	DrawableObject bullet = this.drawableObjectCreator.createDrawableObject("bullet");
	bullet.setLocationX(this.shipHandler.getShip().getFrontX());
	bullet.setLocationY(this.shipHandler.getShip().getFrontY());
	bullet.setAngle(this.shipHandler.getShip().getCurrentAngel() + 90);
	bullet.createBullet();
	this.bulletList.add(bullet);
		
	}

	public void updateBullet() {
		
		this.bulletIterator = bulletList.iterator();
		this.asteroidIterator = asteroidList.iterator();
		
		while(bulletIterator.hasNext()) {
			DrawableObject bullet = bulletIterator.next();
			bullet.move();
			
			while(asteroidIterator.hasNext()) {
				
				DrawableObject ast = asteroidIterator.next();
			
				if(bullet.getShape().getBounds().intersects(ast.getShape().getBounds())) {
					System.out.println("koliderar");
					bulletIterator.remove();
					asteroidIterator.remove();
				}
			}
			
			if(bullet.getLocationX() > 1000 || bullet.getLocationX() < 0 || bullet.getLocationY() > 1000 || bullet.getLocationY() < 0 ) {
				bulletIterator.remove();
			}
		}
		
		
	}

}