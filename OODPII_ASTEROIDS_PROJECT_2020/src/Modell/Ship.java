package Modell;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;


public class Ship {

	private Polygon ship;
	

	
	private boolean isAlive = true;
	private Point2D.Double p1 = new Point2D.Double();
	private Point2D.Double p2 = new Point2D.Double();
	private Point2D.Double p3 = new Point2D.Double();

	private int[] shipx;
	private int[] shipy;

	private double centerX;
	private double centerY;

	private double currentAngle = 270;
	private double bulletAngle = 270;
	
	public Ship() {
		super();
		

		this.p1.x = 510;
		this.p1.y = 450;

		this.p2.x = 520;
		this.p2.y = 420;

		this.p3.x = 530;
		this.p3.y = 450;

		this.shipx = new int[] { (int) this.p1.x, (int) this.p2.x, (int) this.p3.x };
		this.shipy = new int[] { (int) this.p1.y, (int) this.p2.y, (int) this.p3.y };

		this.ship = new Polygon(shipx, shipy, shipx.length);

		this.centerX = (p1.x + p2.x + p3.x) / 3;
		this.centerY = (p1.y + p2.y + p3.y) / 3;
	}

	public Rectangle getBounds() {

		Rectangle rect;
		rect = ship.getBounds();
		return rect;

	}

	public Polygon getShip() {
		return this.ship;
	}

	public int[] getShipx() {
		return shipx;
	}

	public void setShipx(int[] shipx) {
		this.shipx = shipx;
	}

	public int[] getShipy() {
		return shipy;
	}

	public void setShipy(int[] shipy) {
		this.shipy = shipy;
	}

	public int getFrontX() {
		return (int) this.p2.x;
	}

	public int getFrontY() {
		return (int) this.p2.y;
	}

	public double getCenterX() {
		return centerX;
	}

	public void setCenterX(double centerX) {
		this.centerX = centerX;
	}

	public double getCenterY() {
		return centerY;
	}

	public void setCenterY(double centerY) {
		this.centerY = centerY;
	}


	public void moveForward() {

		p1.y = p1.y + (3 * Math.sin(Math.toRadians(currentAngle)));
		p1.x = p1.x + (3 * Math.cos(Math.toRadians(currentAngle)));

		p2.y = p2.y + (3 * Math.sin(Math.toRadians(currentAngle)));
		p2.x = p2.x + (3 * Math.cos(Math.toRadians(currentAngle)));

		p3.y = p3.y + (3 * Math.sin(Math.toRadians(currentAngle)));
		p3.x = p3.x + (3 * Math.cos(Math.toRadians(currentAngle)));

		this.centerX = (p1.x + p2.x + p3.x) / 3;
		this.centerY = (p1.y + p2.y + p3.y) / 3;

		this.shipx = new int[] { (int) this.p1.x, (int) this.p2.x, (int) this.p3.x };
		this.shipy = new int[] { (int) this.p1.y, (int) this.p2.y, (int) this.p3.y };

		this.ship = new Polygon(shipx, shipy, shipx.length);
	}

	public void moveBackward() {

		p1.y = p1.y - (3 * Math.sin(Math.toRadians(currentAngle)));
		p1.x = p1.x - (3 * Math.cos(Math.toRadians(currentAngle)));

		p2.y = p2.y - (3 * Math.sin(Math.toRadians(currentAngle)));
		p2.x = p2.x - (3 * Math.cos(Math.toRadians(currentAngle)));

		p3.y = p3.y - (3 * Math.sin(Math.toRadians(currentAngle)));
		p3.x = p3.x - (3 * Math.cos(Math.toRadians(currentAngle)));

		this.centerX = (p1.x + p2.x + p3.x) / 3;
		this.centerY = (p1.y + p2.y + p3.y) / 3;

		this.shipx = new int[] { (int) this.p1.x, (int) this.p2.x, (int) this.p3.x };
		this.shipy = new int[] { (int) this.p1.y, (int) this.p2.y, (int) this.p3.y };

		this.ship = new Polygon(shipx, shipy, shipx.length);
	}

	public void RotateRight() {

		AffineTransform at = new AffineTransform();

		this.currentAngle = this.currentAngle + 10;
		this.bulletAngle = this.bulletAngle - 10;
		
		at.rotate(Math.toRadians(10), centerX, centerY);
		at.transform(p1, p1);
		at.transform(p2, p2);
		at.transform(p3, p3);

		this.shipx = new int[] { (int) this.p1.x, (int) this.p2.x, (int) this.p3.x };
		this.shipy = new int[] { (int) this.p1.y, (int) this.p2.y, (int) this.p3.y };

		this.ship = new Polygon(shipx, shipy, shipx.length);
	}

	public void RotateLeft() {

		AffineTransform at = new AffineTransform();

		this.currentAngle = this.currentAngle - 10;
		this.bulletAngle = this.bulletAngle + 10;

		at.rotate(Math.toRadians(-10), centerX, centerY);
		at.transform(p1, p1);
		at.transform(p2, p2);
		at.transform(p3, p3);

		this.shipx = new int[] { (int) this.p1.x, (int) this.p2.x, (int) this.p3.x };
		this.shipy = new int[] { (int) this.p1.y, (int) this.p2.y, (int) this.p3.y };

		this.ship = new Polygon(shipx, shipy, shipx.length);
	}

	public double getAngle() {
		return bulletAngle;
	}

	public void setAngle(double bulletAngle) {
		this.bulletAngle = bulletAngle;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	
	public void draw(Graphics g) {
		
		Graphics2D g2d = (Graphics2D) g;
		AffineTransform tx = new AffineTransform();
		
		Shape polygon = new Polygon(this.getShipx(), this.getShipy(),3);
		
		g2d.setColor(Color.RED);
		g2d.draw(tx.createTransformedShape(polygon));
		
	}

	public Shape getShape() {
		// TODO Auto-generated method stub
		return (Shape) this.ship;
	}
	
	public double getCurrentAngel() {
		return this.currentAngle;
	}


}

