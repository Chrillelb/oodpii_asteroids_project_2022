package Modell;

public class DrawableObjectCreator {
	
	public DrawableObject createDrawableObject(String dObject) {
		
		if(dObject.equals("asteroid")) {
			return new Asteroid();
		}
		else if (dObject.equals("bullet")) {
			return new Bullet();
		}
		
		
		return null;
	}

}
