package Modell;

public class ShipHandler {

	private Ship ship;
	
	
	private static ShipHandler handler;
	
	private ShipHandler() {
		this.ship = new Ship();
	}
	
	public static ShipHandler getInstance() {
		if(handler == null) {
			handler =new ShipHandler();
		} 
		return handler;
	}
	
	public void RotateLeft() {	
		ship.RotateLeft();
	}
	public void RotateRight() {
		ship.RotateRight();
	}
	public void moveForward() {
		ship.moveForward();
	}
	public void moveBackWards() {
		ship.moveBackward();
	}
	
	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}
}
